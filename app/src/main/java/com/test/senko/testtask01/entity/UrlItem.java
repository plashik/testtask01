package com.test.senko.testtask01.entity;

import android.database.Cursor;

import static com.test.senko.testtask01.DatabaseHelper.TABLE_URLS_COLOMN_CODE;
import static com.test.senko.testtask01.DatabaseHelper.TABLE_URLS_COLOMN_ID;
import static com.test.senko.testtask01.DatabaseHelper.TABLE_URLS_COLOMN_SAVED_TEXT;

/**
 * Created by Senko on 11/7/2016.
 */

public class UrlItem {

    private int mId;
    private String mText;
    private int mCode;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        mCode = code;
    }

    public boolean isUrl() {
        return android.util.Patterns.WEB_URL.matcher(mText).matches();
    }

    public boolean isChecked() {
        return mCode > 0;
    }

    public static boolean isUrl(String url) {
        return android.util.Patterns.WEB_URL.matcher(url).matches();
    }

    public static UrlItem fromCursor(Cursor cursor) {
        UrlItem item = new UrlItem();
        item.setId(cursor.getInt(TABLE_URLS_COLOMN_ID));
        item.setText(cursor.getString(TABLE_URLS_COLOMN_SAVED_TEXT));
        item.setCode(cursor.getInt(TABLE_URLS_COLOMN_CODE));
        return item;
    }
}