package com.test.senko.testtask01.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.test.senko.testtask01.R;
import com.test.senko.testtask01.fragments.ResultsListFragment;
import com.test.senko.testtask01.fragments.ScannerFragment;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class ScannerActivity extends BaseActivity implements ZBarScannerView.ResultHandler {

    private final int CODE_REQUEST_CAMERA_PERMISSION = 1337;

    private ResultsListFragment mResultsFragment;
    private String mPrevResult;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scanner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mResultsFragment = new ResultsListFragment();
        if (hasCameraPermission()) {
            getFragmentManager().beginTransaction().replace(R.id.scanner_container, new ScannerFragment()).commit();
        }
        getFragmentManager().beginTransaction().replace(R.id.results_container, mResultsFragment).commit();
    }

    @Override
    public void handleResult(Result result) {
        if (!result.getContents().equals(mPrevResult)) {
            mPrevResult = result.getContents();
            mResultsFragment.onNewScanResult(result.getContents());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case CODE_REQUEST_CAMERA_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getFragmentManager().beginTransaction().replace(R.id.scanner_container, new ScannerFragment()).commit();
                } else {
                    requestCamera();
                }
            }
        }
    }

    private boolean hasCameraPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
                //todo
            } else {
                requestCamera();
            }
            return false;
        }
        return true;
    }

    private void requestCamera() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                CODE_REQUEST_CAMERA_PERMISSION);
    }
}
