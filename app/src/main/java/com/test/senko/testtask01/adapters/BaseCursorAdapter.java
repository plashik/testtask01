package com.test.senko.testtask01.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Senko on 11/7/2016.
 */

public abstract class BaseCursorAdapter<VH extends RecyclerView.ViewHolder, T> extends CursorRecyclerViewAdapter<VH> {

    private onItemClickListener<T, VH> mListener;


    BaseCursorAdapter(Context context, Cursor cursor, onItemClickListener<T, VH> listener){
        super(context,cursor);
        mListener = listener;
    }

    protected abstract T itemFromCursor(Cursor cursor);

    protected T setupItem(final VH holder, Cursor cursor) {
        final T item = itemFromCursor(cursor);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onItemClick(item, holder);
                }
            }
        });
        return item;
    }

    public interface onItemClickListener<T, VH>  {
        void onItemClick(T item, VH holder);
    }
}
