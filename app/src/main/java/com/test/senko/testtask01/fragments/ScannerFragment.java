package com.test.senko.testtask01.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.senko.testtask01.view.QRScannerView;

import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by Senko on 11/7/2016.
 */

public class ScannerFragment extends BaseFragment {

    private QRScannerView mScannerView;

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        if (getActivity() instanceof ZBarScannerView.ResultHandler) {
            mScannerView = new QRScannerView(getActivity(), (ZBarScannerView.ResultHandler) getActivity());
        } else {
            throw new RuntimeException("Parent activity must implement ZBarScannerView.ResultHandler interface");
        }
        return mScannerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.startCamera();
    }

    @Override
    public void onStop() {
        super.onStop();
        mScannerView.stopCamera();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
