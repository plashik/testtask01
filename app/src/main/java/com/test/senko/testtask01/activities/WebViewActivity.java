package com.test.senko.testtask01.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.test.senko.testtask01.R;

/**
 * Created by Senko on 11/8/2016.
 */

public class WebViewActivity extends BaseActivity {

    public static final String ARG_URL = "WebViewActivity.ARG_URL";

    private WebView mWebView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_webview;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview);
        mWebView = (WebView) findViewById(R.id.web_view);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.toString());
                return false;
            }
        });
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(ARG_URL)) {
            mWebView.loadUrl(intent.getStringExtra(ARG_URL));
        } else {
            finish();
        }
    }
}
