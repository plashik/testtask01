package com.test.senko.testtask01.view;

import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;

import me.dm7.barcodescanner.core.DisplayUtils;
import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by Senko on 3/7/2015.
 */
public class QRScannerView extends ZBarScannerView {

    private ImageScanner mScanner;
    private ResultHandler mResultHandler;

    public QRScannerView(Context context, ResultHandler resultHandler) {
        super(context);
        setResultHandler(resultHandler);
    }

    public QRScannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    public void setupScanner() {
        this.mScanner = new ImageScanner();
        this.mScanner.setConfig(0, 256, 3);
        this.mScanner.setConfig(0, 257, 3);
        this.mScanner.setConfig(0, 0, 0);
        Iterator i$ = this.getFormats().iterator();

        while (i$.hasNext()) {
            BarcodeFormat format = (BarcodeFormat) i$.next();
            this.mScanner.setConfig(format.getId(), 0, 1);
        }

    }

    @Override
    public void setResultHandler(ZBarScannerView.ResultHandler resultHandler) {
        this.mResultHandler = resultHandler;
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (this.mResultHandler != null) {
            try {
                Camera.Parameters e = camera.getParameters();
                Camera.Size size = e.getPreviewSize();
                int width = size.width;
                int height = size.height;
                int result;
                if (DisplayUtils.getScreenOrientation(this.getContext()) == 1) {
                    byte[] barcode = new byte[data.length];
                    result = 0;

                    while (true) {
                        if (result >= height) {
                            result = width;
                            width = height;
                            height = result;
                            data = barcode;
                            break;
                        }

                        for (int syms = 0; syms < width; ++syms) {
                            barcode[syms * height + height - result - 1] = data[syms + result * width];
                        }

                        ++result;
                    }
                }

                Image var15 = new Image(width, height, "Y800");
                var15.setData(data);
                result = this.mScanner.scanImage(var15);
                if (result != 0) {
                    SymbolSet var16 = this.mScanner.getResults();
                    final Result rawResult = new Result();
                    Iterator handler = var16.iterator();

                    while (handler.hasNext()) {
                        Symbol sym = (Symbol) handler.next();
                        String symData;
                        if (Build.VERSION.SDK_INT >= 19) {
                            symData = new String(sym.getDataBytes(), StandardCharsets.UTF_8);
                        } else {
                            symData = sym.getData();
                        }

                        if (!TextUtils.isEmpty(symData)) {
                            rawResult.setContents(symData);
                            rawResult.setBarcodeFormat(BarcodeFormat.getFormatById(sym.getType()));
                            break;
                        }
                    }

                    Handler var17 = new Handler(Looper.getMainLooper());
                    var17.post(new Runnable() {
                        public void run() {
                            //ZBarScannerView.ResultHandler tmpResultHandler = mResultHandler;
                            if (mResultHandler != null) {
                                mResultHandler.handleResult(rawResult);
                            }

                        }
                    });

                }
                camera.setOneShotPreviewCallback(this);

            } catch (RuntimeException var14) {
                Log.e("ZBarScannerView", var14.toString(), var14);
            }
        }
    }

}
