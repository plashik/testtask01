package com.test.senko.testtask01.network;

import android.support.annotation.NonNull;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

/**
 * Created by Senko on 11/8/2016.
 */

public class VolleyCodeCBWrapper {


    private final CodeRequest mRequest;
    private final VolleyCodeCallback mCb;

    public VolleyCodeCBWrapper(String url, @NonNull VolleyCodeCallback cb) {
        mRequest = new CodeRequest(url, mSuccessListener, mErrorListener);
        mCb = cb;
    }

    public CodeRequest getRequest() {
        return mRequest;
    }

    private Response.Listener<String> mSuccessListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            mCb.onResult(mRequest.getStatusCode());
        }
    };

    private Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            mCb.onResult(error.networkResponse.statusCode);
        }
    };


    private class CodeRequest extends StringRequest {

        private int mStatusCode;

        CodeRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
            super(url, listener, errorListener);
        }

        @Override
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            mStatusCode = response.statusCode;
            return super.parseNetworkResponse(response);
        }

        int getStatusCode() {
            return mStatusCode;
        }
    }

    public interface VolleyCodeCallback {
        void onResult(int statusCode);
    }
}
