package com.test.senko.testtask01.adapters;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.test.senko.testtask01.entity.UrlItem;
import com.test.senko.testtask01.R;

/**
 * Created by Senko on 11/7/2016.
 */

public class UrlRecyclerAdapter extends BaseCursorAdapter<UrlRecyclerAdapter.ViewHolder, UrlItem> {

    public UrlRecyclerAdapter(Context context, Cursor cursor, onItemClickListener listener){
        super(context, cursor, listener);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {
        TextView mTextUrl;
        public TextView mTextCode;
        public ProgressBar mPb;
        ViewHolder(View view) {
            super(view);
            mTextUrl = (TextView) view.findViewById(R.id.list_item_url);
            mTextCode = (TextView) view.findViewById(R.id.list_item_code);
            mPb = (ProgressBar) view.findViewById(R.id.list_item_pb);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    protected UrlItem itemFromCursor(Cursor cursor) {
        return UrlItem.fromCursor(cursor);
    }

    @Override
    public void onBindViewHolder(UrlRecyclerAdapter.ViewHolder holder, Cursor cursor) {
        UrlItem item = setupItem(holder, cursor);
        setupViewHolder(holder, item);
    }

    public static void setupViewHolder(UrlRecyclerAdapter.ViewHolder holder, UrlItem item) {
        holder.mTextUrl.setText(item.getText());
        if (item.isUrl()) {
            holder.mTextCode.setText(String.valueOf(item.getCode()));
            holder.mPb.setVisibility(View.GONE);
            holder.mTextCode.setVisibility(View.VISIBLE);
        } else {
            holder.mTextCode.setVisibility(View.INVISIBLE);
        }
    }
}