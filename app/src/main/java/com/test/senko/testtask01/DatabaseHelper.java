package com.test.senko.testtask01;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.test.senko.testtask01.entity.UrlItem;

/**
 * Created by Senko on 11/2/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "urls";
    private static final String TABLE_URLS = "saved_urls";

    private static final String TABLE_URLS_SAVED_ID = "_id";
    private static final String TABLE_URLS_SAVED_URLS = "saved_urls";
    private static final String TABLE_URLS_SAVED_CODE = "code";

    private static final String SELECT_ALL_QUERY = "SELECT * FROM " + TABLE_URLS + " ORDER BY _id ASC";

    public static final int TABLE_URLS_COLOMN_ID = 0;
    public static final int TABLE_URLS_COLOMN_SAVED_TEXT = 1;
    public static final int TABLE_URLS_COLOMN_CODE = 2;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_URLS +
                "( " + TABLE_URLS_SAVED_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TABLE_URLS_SAVED_URLS + " TEXT NOT NULL UNIQUE, " + TABLE_URLS_SAVED_CODE + " INTEGER)";

        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //todo
    }

    public boolean insert(String text, int code) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(TABLE_URLS_SAVED_URLS, text);
            values.put(TABLE_URLS_SAVED_CODE, code);
            db.insert(TABLE_URLS, null, values);
        } catch (SQLiteConstraintException ex) {
            return false;
        }
        db.close();
        return true;
    }

    public boolean insert(String text) {
        return insert(text, 0);
    }

    public boolean save(UrlItem item) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(TABLE_URLS_SAVED_URLS, item.getText());
            values.put(TABLE_URLS_SAVED_CODE, item.getCode());
            db.update(TABLE_URLS, values, TABLE_URLS_SAVED_ID + "=?", new String[] {String.valueOf(item.getId())});
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        db.close();
        return true;
    }

    public static Cursor getCursor(Context context) {
        DatabaseHelper helper = new DatabaseHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        return db.rawQuery(SELECT_ALL_QUERY, null);
    }
}