package com.test.senko.testtask01.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by Senko on 11/7/2016.
 */

public class ActivityController {

    private final AppCompatActivity mActivity;

    public ActivityController(@NonNull AppCompatActivity activity) {
        mActivity = activity;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        ActionBar actionBar = mActivity.getSupportActionBar();
        if (mActivity instanceof ScannerActivity) {
            if (actionBar != null) {
                actionBar.hide();
            }
        } else
        if (mActivity instanceof WebViewActivity) {
            if (actionBar != null) {
                actionBar.setHomeButtonEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.show();
            }
        }
    }

    public void onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (mActivity instanceof WebViewActivity) {
                    NavUtils.navigateUpFromSameTask(mActivity);
                }
        }
    }
}
