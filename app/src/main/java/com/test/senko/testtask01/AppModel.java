package com.test.senko.testtask01;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.test.senko.testtask01.activities.WebViewActivity;

/**
 * Created by Senko on 11/7/2016.
 */

public class AppModel {
    public static final int INTENT_VIEW_URL = 0x00001;

    public static Intent buildIntent(Context context, int intentId, Bundle arguments, int flags) {
        Intent intent = new Intent();
        switch (intentId) {
            case INTENT_VIEW_URL:
                intent.setClass(context, WebViewActivity.class);
                break;
        }
        intent.setFlags(flags);
        if (arguments != null) {
            intent.putExtras(arguments);
        }
        return intent;
    }

    public static void sendBroadcast(Context context, String action, Bundle data) {
        if (context != null) {
            Intent intent = new Intent(action);
            if (data != null) {
                intent.putExtras(data);
            }
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }

    public static class BB {
        private Bundle mBundle;
        public BB() {
            mBundle = new Bundle();
        }

        public Bundle putString(String key, String value){
            mBundle.putString(key, value);
            return mBundle;
        }
    }
}
