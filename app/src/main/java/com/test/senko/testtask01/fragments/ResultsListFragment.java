package com.test.senko.testtask01.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.test.senko.testtask01.AppController;
import com.test.senko.testtask01.AppModel;
import com.test.senko.testtask01.DatabaseHelper;
import com.test.senko.testtask01.R;
import com.test.senko.testtask01.activities.WebViewActivity;
import com.test.senko.testtask01.adapters.BaseCursorAdapter;
import com.test.senko.testtask01.adapters.UrlRecyclerAdapter;
import com.test.senko.testtask01.entity.UrlItem;
import com.test.senko.testtask01.network.VolleyCodeCBWrapper;

/**
 * Created by Senko on 11/7/2016.
 */

public class ResultsListFragment extends BaseFragment implements BaseCursorAdapter.onItemClickListener<UrlItem, UrlRecyclerAdapter.ViewHolder> {

    private static final String BROADCAST_NEW_DATA = "ResultsListFragment.BROADCAST_NEW_DATA";

    protected RecyclerView mRecycler;
    private UrlRecyclerAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_urls;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_NEW_DATA);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mActionReceiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mActionReceiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        mRecycler = (RecyclerView) root.findViewById(R.id.recycler_view);
        mAdapter = new UrlRecyclerAdapter(getActivity(), DatabaseHelper.getCursor(getActivity()), this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setAdapter(mAdapter);
        return root;
    }


    public void onNewScanResult(final String contents) {
        if (contents != null) {
            if (UrlItem.isUrl(contents)) {
                VolleyCodeCBWrapper callback = new VolleyCodeCBWrapper(contents, new VolleyCodeCBWrapper.VolleyCodeCallback() {
                    @Override
                    public void onResult(int statusCode) {
                        AppController.getInstance().getDatabase().insert(contents, statusCode);
                        AppModel.sendBroadcast(getAppContext(), BROADCAST_NEW_DATA, null);
                    }
                });
                AppController.getInstance().getVolleyQueue().add(callback.getRequest());
            } else {
                AppController.getInstance().getDatabase().insert(contents);
                AppModel.sendBroadcast(getAppContext(), BROADCAST_NEW_DATA, null);
            }
        }
    }

    @Override
    public void onItemClick(final UrlItem item, final UrlRecyclerAdapter.ViewHolder holder) {
        if (item.isUrl()) {
            startActivity(AppModel.buildIntent(getActivity(), AppModel.INTENT_VIEW_URL,
                    new AppModel.BB().putString(WebViewActivity.ARG_URL, item.getText()), 0));
        }
    }

    private BroadcastReceiver mActionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equals(BROADCAST_NEW_DATA)) {
                if (isAdded()) {
                    mAdapter.swapCursor(DatabaseHelper.getCursor(getActivity())).close();
                    mRecycler.smoothScrollToPosition(mAdapter.getCursor().getCount());
                    Toast.makeText(getActivity(), "BROABCAST", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

}

