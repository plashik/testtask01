package com.test.senko.testtask01.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Senko on 11/7/2016.
 */

public abstract class BaseFragment extends Fragment {

    protected abstract int getLayoutId();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), null);
    }

    protected Context getAppContext() {
        if (getActivity() != null) {
            return getActivity().getApplicationContext();
        }
        return null;
    }
}
