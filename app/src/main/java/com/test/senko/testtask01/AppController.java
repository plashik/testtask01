package com.test.senko.testtask01;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Senko on 11/8/2016.
 */

public class AppController extends Application {

    private static AppController sInstance;
    private DatabaseHelper mDatabase;
    private RequestQueue mQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static synchronized AppController getInstance() {
        return sInstance;
    }


    public RequestQueue getVolleyQueue() {
        if (mQueue == null) {
            mQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mQueue;
    }

    public DatabaseHelper getDatabase() {
        if (mDatabase == null) {
            mDatabase = new DatabaseHelper(getApplicationContext());
        }
        return mDatabase;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mDatabase != null) {
            mDatabase.close();
        }
    }
}
