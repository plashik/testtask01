package com.test.senko.testtask01.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by Senko on 11/7/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {


    private ActivityController mController;

    protected abstract int getLayoutId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mController = new ActivityController(this);
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        mController.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mController.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }
}
